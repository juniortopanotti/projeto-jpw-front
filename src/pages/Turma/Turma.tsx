import React from "react";
import { Button, ButtonGroup, Col, Form, Row, Spinner } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import { SubmitHandler, useForm } from "react-hook-form";
import Header from "../../components/Header";
import { Turma } from "../../model/turma/turma";
import TurmaService from "../../services/turma";
import { useMutation, useQuery } from "react-query";
import Body from "../../components/Body";
import { toast } from "react-toastify";
import {CustomForm} from './styles'
import { turmaListRoutePath } from "../../routes/turma.route";

const Detail: React.FC = () => {
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  useQuery("fase", carregaTurma, {
    enabled: !!id,
  });

  const mutation = useMutation(
    async (data: Turma) => {
      if (data._id) {
        await TurmaService.update(data);
      } else {
        await TurmaService.save(data);
      }
    },
    {
      onError: (error: any) => {
        toast.error(error.message);
      },
      onSuccess: () => {
        toast.success("Turma salva");
        history.push(turmaListRoutePath);
      },
    }
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<Turma>();
  const onSubmit: SubmitHandler<Turma> = async (data: Turma) => {
    await mutation.mutateAsync(data);
  };

  async function carregaTurma() {
    const response: any = await TurmaService.get(id);

    reset(response.data);

    return response.data;
  }

  return (
    <Body>
      <Row className="header align-items-center pr-2 pl-2">
        <Header
          title={"Turma"}
          subtitle={""}
        />
        <Col className="text-right">
          <ButtonGroup className="float-right">
            <Button className="float-right" variant="secondary" onClick={handleSubmit(onSubmit)}>
              {mutation.isLoading ? <Spinner animation={"border"} /> : "Salvar"}
            </Button>
          </ButtonGroup>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <CustomForm>
          <Form.Group controlId="descricao">
            <Form.Label>Descrição</Form.Label>
            <Form.Control
              {...register("descricao")}
              name="descricao"
              isInvalid={!!errors.descricao}
              placeholder="Descrição"
            />
            {errors.descricao && (
              <Form.Control.Feedback type="invalid">
                {/* {errors?.descricao?.message} */}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group controlId="ano">
            <Form.Label>Ano</Form.Label>
            <Form.Control
              {...register("ano")}
              name="ano"
              type="number"
              isInvalid={!!errors.ano}
              placeholder="Ano"
            />
            {errors.ano && (
              <Form.Control.Feedback type="invalid">
                {errors?.ano?.message}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group controlId="fase">
            <Form.Label>Fase</Form.Label>
            <Form.Control
              {...register("fase")}
              name="fase"
              type="number"
              isInvalid={!!errors.fase}
              placeholder="Fase"
            />
            {errors.fase && (
              <Form.Control.Feedback type="invalid">
                {errors?.fase?.message}
              </Form.Control.Feedback>
            )}
          </Form.Group>
        </CustomForm>
      </Row>
    </Body>
  );
};

export { Detail };
