import React from "react";
import {
  Button,
  ButtonGroup,
  Col,
  Dropdown,
  Row,
  Spinner,
  Form,
  Table,
} from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import Body from "../../components/Body";
import { StyledTh } from "./styles";
import { useMutation, useQuery } from "react-query";
import TurmaService from "../../services/turma";
import { ReactComponent as More } from "../../assets/icons/more.svg";
import Header from "../../components/Header";
import { toast } from "react-toastify";
import { turmaListRoutePath } from "../../routes/turma.route";

const List: React.FC = () => {
  const [filtro, setFiltro] = React.useState('')
  const { data, isLoading, refetch } = useQuery(["fase", filtro], () => carregaTurma(filtro));
  const history = useHistory();

  async function carregaTurma(filtro: any) {
    const response: any = await TurmaService.filtrar(filtro);
    return response.data;
  }

  async function filtrar(nome: any) {
    setFiltro(nome)
  }

  const mutation = useMutation(
    async (id: number) => {
      await TurmaService.delete(id);
    },
    {
      onError: (error: any) => {
        toast.error(error.message);
      },
      onSuccess: () => {
        toast.success("Turma excluido");
      },
    }
  );

  return (
    <Body>
      <Row className="header align-items-center pr-2 pl-2">
        <Header title={"Turmas"} subtitle={""} />
        <Col className="text-right">
          <ButtonGroup className="float-right">
            <Button
              variant="secondary"
              className="float-right"
              onClick={() => {
                history.push(`${turmaListRoutePath}/novo`);
              }}
            >
              +
            </Button>
          </ButtonGroup>
        </Col>
      </Row>
      <Row className="header align-items-center pr-2 pl-2">
        <Form>
          <Form.Row>
            <Form.Group as={Col} controlId="formGridEmail">
              <Form.Control type="text" placeholder="Fase" onChange={e => filtrar(e.target.value )}/>
            </Form.Group>
          </Form.Row>
        </Form>
      </Row>
      <Row>
        <Table>
          <thead>
            <tr>
              <StyledTh>ID</StyledTh>
              <StyledTh>DESCRIÇÃO</StyledTh>
              <StyledTh>ANO</StyledTh>
              <StyledTh>FASE</StyledTh>
              <StyledTh></StyledTh>
            </tr>
          </thead>
          <tbody>
            {isLoading || !data.map ? (
              <Spinner animation={"border"} />
            ) : (
              data?.map((item: any, index: number) => (
                <tr key={index}>
                  <td>{item._id}</td>
                  <td>{item.descricao}</td>
                  <td>{item.ano}</td>
                  <td>{item.fase}</td>
                  <td>
                    <Dropdown className={"float-right"} key="left">
                      <Dropdown.Toggle
                        bsPrefix="nexen"
                        as={Button}
                        variant="text"
                      >
                        <More />
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item
                          as={Link}
                          to={`${turmaListRoutePath}/${item._id}/editar`}
                        >
                          Editar
                        </Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item
                          onClick={async () => {
                            await mutation.mutateAsync(item._id);
                            refetch();
                          }}
                        >
                          Excluir
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </Table>
      </Row>
    </Body>
  );
};

export { List };
