import React from "react";
import { Button, Form, Spinner } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { SubmitHandler, useForm } from "react-hook-form";
import { ILogin } from "../../model/login/login";
import LoginService from "../../services/login";
import { Div } from "./styles";
import { toast } from "react-toastify";
import { useMutation } from "react-query";
import { estudanteListRoutePath } from "../../routes/estudante.route";

const Login: React.FC = () => {
  const history = useHistory();
  const mutation = useMutation(
    async (data: ILogin) => {
      await LoginService.login(data.email, data.senha);
    },
    {
      onError: (error: any) => {
        toast.error("Usuário ou senha inválidos!");
      },
      onSuccess: () => {
        history.push(estudanteListRoutePath);
      },
    }
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ILogin>();
  const onSubmit: SubmitHandler<ILogin> = (data: ILogin) =>
    mutation.mutate(data);

  return (
    <>
    <Div>
      <Form className="mt-5" onSubmit={handleSubmit(onSubmit)}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            {...register("email")}
            name="email"
            isInvalid={!!errors.email}
            placeholder="Email"
          />
          {errors.email && (
            <Form.Control.Feedback type="invalid">
              {errors?.email?.message}
            </Form.Control.Feedback>
          )}
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Senha</Form.Label>
          <Form.Control
            {...register("senha")}
            type="password"
            name="senha"
            placeholder="Senha"
          />
        </Form.Group>

        <Button className={"mt-5shadow-none"} block type="submit">
          {mutation.isLoading ? (
            <Spinner animation={"border"} />
          ) : (
            "Entrar"
          )}
        </Button>
      </Form>
      </Div>
    </>
  );
};

export default Login;
