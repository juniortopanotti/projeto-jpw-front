import { Form } from "react-bootstrap";
import styled from "styled-components";

export const StyledTh = styled.th`
  background-color: #f2f2f2;
`;

export const CustomForm = styled(Form)`
  width: 100%;
`;
