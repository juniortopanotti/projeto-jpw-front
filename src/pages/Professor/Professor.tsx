import React from "react";
import { Button, ButtonGroup, Col, Form, Row, Spinner } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import { SubmitHandler, useForm } from "react-hook-form";
import Header from "../../components/Header";
import { Professor } from "../../model/professor/professor";
import ProfessorService from "../../services/professor";
import { useMutation, useQuery } from "react-query";
import Body from "../../components/Body";
import { toast } from "react-toastify";
import { professorListRoutePath } from "../../routes/professor.route";
import { CustomForm } from './styles'

const Detail: React.FC = () => {
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  useQuery("nome", loadProfessor, {
    enabled: !!id,
  });

  const mutation = useMutation(
    async (data: Professor) => {
      if (data._id) {
        await ProfessorService.update(data);
      } else {
        await ProfessorService.save(data);
      }
    },
    {
      onError: (error: any) => {
        toast.error(error.message);
      },
      onSuccess: () => {
        toast.success("Professor salvo");
        history.push(professorListRoutePath);
      },
    }
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<Professor>();
  const onSubmit: SubmitHandler<Professor> = async (data: Professor) => {
    await mutation.mutateAsync(data);
  };

  async function loadProfessor() {
    const response: any = await ProfessorService.get(id);

    reset(response.data);

    return response.data;
  }

  return (
    <Body>
      <Row className="header align-items-center pr-2 pl-2">
        <Header
          title={"Professores"}
          subtitle={""}
        />
        <Col className="text-right">
          <ButtonGroup className="float-right">
            <Button className="float-right" variant="secondary" onClick={handleSubmit(onSubmit)}>
              {mutation.isLoading ? <Spinner animation={"border"} /> : "Salvar"}
            </Button>
          </ButtonGroup>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <CustomForm>
          <Form.Group controlId="nome">
            <Form.Label>Nome</Form.Label>
            <Form.Control
              {...register("nome")}
              name="nome"
              isInvalid={!!errors.nome}
              placeholder="Nome"
            />
            {errors.nome && (
              <Form.Control.Feedback type="invalid">
                {errors?.nome?.message}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group controlId="especialidade">
            <Form.Label>Especialidade</Form.Label>
            <Form.Control
              {...register("especialidade")}
              name="especialidade"
              isInvalid={!!errors.especialidade}
              placeholder="Especialidade"
            />
            {errors.especialidade && (
              <Form.Control.Feedback type="invalid">
                {errors?.especialidade?.message}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group controlId="cpf">
            <Form.Label>CPF</Form.Label>
            <Form.Control
              {...register("cpf")}
              name="cpf"
              isInvalid={!!errors.cpf}
              placeholder="CPF"
            />
            {errors.cpf && (
              <Form.Control.Feedback type="invalid">
                {errors?.cpf?.message}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group controlId="dt_nascimento">
            <Form.Label>Data de nascimento</Form.Label>
            <Form.Control
              {...register("dt_nascimento")}
              name="dt_nascimento"
              isInvalid={!!errors.dt_nascimento}
              placeholder="Data de nascimento"
            />
            {errors.dt_nascimento && (
              <Form.Control.Feedback type="invalid">
                {errors?.dt_nascimento?.message}
              </Form.Control.Feedback>
            )}
          </Form.Group>
        </CustomForm>
      </Row>
    </Body>
  );
};

export { Detail };
