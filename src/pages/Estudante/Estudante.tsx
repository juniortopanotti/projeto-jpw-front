import React from "react";
import { Button, ButtonGroup, Col, Form, Row, Spinner } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import { SubmitHandler, useForm } from "react-hook-form";
import Header from "../../components/Header";
import { Estudante } from "../../model/estudante/estudante";
import EstudanteService from "../../services/estudante";
import { useMutation, useQuery } from "react-query";
import Body from "../../components/Body";
import { toast } from "react-toastify";
import { estudanteListRoutePath } from "../../routes/estudante.route";
import { CustomForm } from './styles';

const Detail: React.FC = () => {
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  useQuery("turma", loadEstudante, {
    enabled: !!id,
  });

  const mutation = useMutation(
    async (data: Estudante) => {
      if (data._id) {
        await EstudanteService.update(data);
      } else {
        await EstudanteService.save(data);
      }
    },
    {
      onError: (error: any) => {
        toast.error(error.message);
      },
      onSuccess: () => {
        toast.success("Estudante salvo");
        history.push(estudanteListRoutePath);
      },
    }
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<Estudante>();
  const onSubmit: SubmitHandler<Estudante> = async (data: Estudante) => {
    await mutation.mutateAsync(data);
  };

  async function loadEstudante() {
    const response: any = await EstudanteService.get(id);
    reset(response.data);
    return response.data;
  }

  return (
    <Body>
      <Row className="header align-items-center pr-2 pl-2">
        <Header
          title={"Estudante"}
          subtitle={""}
        />
        <Col className="text-right">
          <ButtonGroup className="float-right">
            <Button className="float-right" variant="secondary" onClick={handleSubmit(onSubmit)}>
              {mutation.isLoading ? <Spinner animation={"border"} /> : "Salvar"}
            </Button>
          </ButtonGroup>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <CustomForm>
          <Form.Group controlId="nome">
            <Form.Label>Nome</Form.Label>
            <Form.Control
              {...register("nome")}
              name="nome"
              isInvalid={!!errors.nome}
              placeholder="Nome"
            />
            {errors.nome && (
              <Form.Control.Feedback type="invalid">
                {errors?.nome}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group controlId="matricula">
            <Form.Label>Matricula</Form.Label>
            <Form.Control
              {...register("matricula")}
              name="matricula"
              isInvalid={!!errors.matricula}
              placeholder="Matricula"
            />
            {errors.matricula && (
              <Form.Control.Feedback type="invalid">
                {errors?.matricula}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group controlId="turma">
            <Form.Label>Turma</Form.Label>
            <Form.Control
              {...register("turma")}
              name="turma"
              isInvalid={!!errors.turma}
              placeholder="Turma"
            />
            {errors.turma && (
              <Form.Control.Feedback type="invalid">
                {errors?.turma}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group controlId="nascimento">
            <Form.Label>Data de nascimento</Form.Label>
            <Form.Control
              {...register("dt_nascimento")}
              name="dt_nascimento"
              isInvalid={!!errors.dt_nascimento}
              placeholder="Data de nascimento"
            />
            {errors.dt_nascimento && (
              <Form.Control.Feedback type="invalid">
                {errors?.dt_nascimento}
              </Form.Control.Feedback>
            )}
          </Form.Group>
        </CustomForm>
      </Row>
    </Body>
  );
};

export { Detail };
