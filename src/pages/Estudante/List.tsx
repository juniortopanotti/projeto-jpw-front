import React from "react";
import {
  Button,
  ButtonGroup,
  Col,
  Dropdown,
  Form,
  Row,
  Spinner,
  Table,
} from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import Body from "../../components/Body";
import { StyledTh } from "./styles";
import { useMutation, useQuery } from "react-query";
import EstudanteService from "../../services/estudante";
import { ReactComponent as More } from "../../assets/icons/more.svg";
import Header from "../../components/Header";
import { toast } from "react-toastify";
import { estudanteListRoutePath } from "../../routes/estudante.route";

const List: React.FC = () => {
  const [filtro, setFiltro] = React.useState('')
  const { data, isLoading, refetch } = useQuery(["turma", filtro], () => carregaEstudante(filtro));
  const history = useHistory();

  async function carregaEstudante(filtro: any) {
    const response: any = await EstudanteService.filtrar(filtro);
    return response.data;
  }

  async function filtrar(nome: any) {
    setFiltro(nome)
  }

  const mutation = useMutation(
    async (id: number) => {
      await EstudanteService.delete(id);
    },
    {
      onError: (error: any) => {
        toast.error(error.message);
      },
      onSuccess: () => {
        toast.success("Estudante excluido");
      },
    }
  );

  return (
    <Body>
      <Row className="header align-items-center pr-2 pl-2">
        <Header title={"Estudantes"} subtitle={""} />
        <Col className="text-right">
          <ButtonGroup className="float-right">
            <Button
              variant="secondary"
              className="float-right"
              onClick={() => {
                history.push(`${estudanteListRoutePath}/novo`);
              }}
            >
              +
            </Button>
          </ButtonGroup>
        </Col>
      </Row>
      <Row className="header align-items-center pr-2 pl-2">
        <Form>
          <Form.Row>
            <Form.Group as={Col} controlId="formGridEmail">
              <Form.Control type="text" placeholder="Turma" onChange={e => filtrar(e.target.value )}/>
            </Form.Group>
          </Form.Row>
        </Form>
      </Row>
      <Row>
        <Table>
          <thead>
            <tr>
              <StyledTh>ID</StyledTh>
              <StyledTh>MATRICULA</StyledTh>
              <StyledTh>NOME</StyledTh>
              <StyledTh>TURMA</StyledTh>
              <StyledTh>NASCIMENTO</StyledTh>
              <StyledTh></StyledTh>
            </tr>
          </thead>
          <tbody>
            {isLoading || !data.map ? (
              <Spinner animation={"border"} />
            ) : (
              data?.map((item: any, index: number) => (
                <tr key={index}>
                  <td>{item._id}</td>
                  <td>{item.matricula}</td>
                  <td>{item.nome}</td>
                  <td>{item.turma}</td>
                  <td>{item.dt_nascimento}</td>

                  <td>
                    <Dropdown className={"float-right"} key="left">
                      <Dropdown.Toggle
                        bsPrefix="nexen"
                        as={Button}
                        variant="text"
                      >
                        <More />
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item
                          as={Link}
                          to={`${estudanteListRoutePath}/${item._id}/editar`}
                        >
                          Editar
                        </Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item
                          onClick={async () => {
                            await mutation.mutateAsync(item._id);
                            refetch();
                          }}
                        >
                          Excluir
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </Table>
      </Row>
    </Body>
  );
};

export { List };
