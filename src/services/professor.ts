import { Professor } from "../model/professor/professor";
import API from "./api";

export default class ProfessorService {
  private static uri = "professor";

  public static filtrar(nome: any) {
    return API.get(this.uri + "?nome=" + nome);
  }

  public static get(id: string) {
    return API.get(`${this.uri}/${id}`);
  }

  public static update(data: Professor) {
    return API.put(`${this.uri}/${data._id}`, data);
  }

  public static save(data: Professor) {
    return API.post(this.uri, data);
  }

  public static async delete(id: number) {
    return await API.delete(`${this.uri}/${id}`);
  }
}
