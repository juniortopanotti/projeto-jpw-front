import API from "./api";

export default class LoginService {
  public static login(email: string, senha: string) {
    return API.post("/auth", {
      email,
      senha,
    });
  }
}
