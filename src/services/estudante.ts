import { Estudante } from "../model/estudante/estudante";
import API from "./api";

export default class EstudanteService {
  private static uri = "estudante";

  public static filtrar(param: string) {
    return API.get(this.uri + "?turma=" + param);
  }

  public static get(id: string) {
    return API.get(`${this.uri}/${id}`);
  }

  public static update(data: Estudante) {
    return API.put(`${this.uri}/${data._id}`, data);
  }

  public static save(data: Estudante) {
    return API.post(this.uri, data);
  }

  public static async delete(id: number) {
    return await API.delete(`${this.uri}/${id}`);
  }
}
