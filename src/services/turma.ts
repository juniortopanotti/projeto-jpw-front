import { Turma } from "../model/turma/turma";
import API from "./api";

export default class TurmaService {
  private static uri = "turma";

  public static filtrar(nome: any) {
    return API.get(this.uri + "?fase=" + nome);
  }

  public static get(id: string) {
    return API.get(`${this.uri}/${id}`);
  }

  public static update(data: Turma) {
    const dataParsed = { ...data, professores: [] };
    return API.put(`${this.uri}/${data._id}`, dataParsed);
  }

  public static save(data: Turma) {
    const dataParsed = { ...data, professores: [] };
    return API.post(this.uri, dataParsed);
  }

  public static async delete(id: number) {
    return await API.delete(`${this.uri}/${id}`);
  }
}
