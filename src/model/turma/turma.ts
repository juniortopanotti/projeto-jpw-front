export interface Turma {
  _id: number;
  descricao: String;
  ano: number;
  fase: number;
  professores: String[];
}
