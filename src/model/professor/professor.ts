export interface Professor {
  _id: number;
  nome: string;
  cpf: string;
  dt_nascimento: string;
  especialidade: string;
}
