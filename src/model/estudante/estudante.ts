export interface Estudante {
  _id: number;
  nome: string;
  matricula: number;
  dt_nascimento: string;
  turma: string;
}
