import React from "react";
import { Route, Switch } from "react-router-dom";
import { List, Detail } from "../pages/Turma";

export const turmaListRoutePath = "/turma";
export const turmaDetalhesRoutePath = "/turma/:id/editar";
export const turmaNovoRoutePath = "/turma/novo";

export const TurmaRoutesComponent: React.FC = () => (
  <Switch>
    <Route path={turmaListRoutePath} component={List} exact />
    <Route path={turmaDetalhesRoutePath} component={Detail} exact />
    <Route path={turmaNovoRoutePath} component={Detail} exact />
  </Switch>
);
