import React from "react";
import { Route, Switch } from "react-router-dom";
import { List, Detail } from "../pages/Professor";

export const professorListRoutePath = "/professor";
export const professorDetalhesRoutePath = "/professor/:id/editar";
export const professorNovoRoutePath = "/professor/novo";

export const ProfessorRoutesComponent: React.FC = () => (
  <Switch>
    <Route path={professorListRoutePath} component={List} exact />
    <Route path={professorDetalhesRoutePath} component={Detail} exact />
    <Route path={professorNovoRoutePath} component={Detail} exact />
  </Switch>
);
