import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { AuthRoutesComponent, loginRoutePath } from "./auth.route";
import { estudanteDetalhesRoutePath, estudanteListRoutePath, estudanteNovoRoutePath, EstudanteRoutesComponent } from "./estudante.route";
import { professorDetalhesRoutePath, professorListRoutePath, professorNovoRoutePath, ProfessorRoutesComponent } from "./professor.route";
import { turmaDetalhesRoutePath, turmaListRoutePath, turmaNovoRoutePath, TurmaRoutesComponent } from "./turma.route";

const AppRoutes: React.FC = () => (
  <Switch>
    <Route exact path="/">
      <Redirect to="/login" />
    </Route>
    <Route path={loginRoutePath} component={AuthRoutesComponent} />
    <Route path={professorListRoutePath} component={ProfessorRoutesComponent} />
    <Route path={professorDetalhesRoutePath} component={ProfessorRoutesComponent} />
    <Route path={professorNovoRoutePath} component={ProfessorRoutesComponent} />
    <Route path={estudanteListRoutePath} component={EstudanteRoutesComponent} />
    <Route path={estudanteDetalhesRoutePath} component={EstudanteRoutesComponent} />
    <Route path={estudanteNovoRoutePath} component={EstudanteRoutesComponent} />
    <Route path={turmaListRoutePath} component={TurmaRoutesComponent}/>
    <Route path={turmaDetalhesRoutePath} component={TurmaRoutesComponent}/>
    <Route path={turmaNovoRoutePath} component={TurmaRoutesComponent}/>
  </Switch>
);

export default AppRoutes;
