import React from "react";
import { Route, Switch } from "react-router-dom";
import { List, Detail } from "../pages/Estudante";

export const estudanteListRoutePath = "/estudante";
export const estudanteDetalhesRoutePath = "/estudante/:id/editar";
export const estudanteNovoRoutePath = "/estudante/novo";

export const EstudanteRoutesComponent: React.FC = () => (
  <Switch>
    <Route path={estudanteListRoutePath} component={List} exact />
    <Route path={estudanteDetalhesRoutePath} component={Detail} exact />
    <Route path={estudanteNovoRoutePath} component={Detail} exact />
  </Switch>
);
