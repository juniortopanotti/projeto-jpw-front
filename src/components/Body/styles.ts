import { Container, Navbar } from "react-bootstrap";
import styled from "styled-components";

export const Menu = styled.div`
  width: 250px;
  background-color: #000080;
  padding: 10px;
  color: #ffffff;
  height: 100vh;
`;

export const Content = styled.div`
  width: 100%;
  height: 100vh;
  padding-left: 30px;
  padding-right: 30px;
`;

export const CustomContainer = styled(Container)`
  display: flex !important;
  justify-content: space-between;
  align-items: stretch;
  flex-direction: row;
  margin: 0;
  padding: 0;
`;
