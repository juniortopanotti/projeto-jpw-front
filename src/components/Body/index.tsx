import React from "react";
import {Menu, CustomContainer, Content} from './styles'
import { Nav} from "react-bootstrap";
import { Link } from "react-router-dom";
import { estudanteListRoutePath } from "../../routes/estudante.route";
import { professorListRoutePath } from "../../routes/professor.route";
import { turmaListRoutePath } from "../../routes/turma.route";

const Body: React.FC = ({ children }) => (
  <>
    <CustomContainer fluid>
      <Menu>
        <Nav className="mr-auto">
          <Nav.Link as={Link} to={estudanteListRoutePath} href="#estudante">
            Estudante
          </Nav.Link>
          <Nav.Link as={Link} to={professorListRoutePath} href="#professor">
            Professores
          </Nav.Link>
          <Nav.Link as={Link} to={turmaListRoutePath} href="#turma">
            Turmas
          </Nav.Link>
        </Nav>
      </Menu>
      <Content>
        {children}
      </Content>
    </CustomContainer>
    <br />
  </>
);

export default Body;
